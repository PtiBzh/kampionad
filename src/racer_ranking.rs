use serde::Serialize;

/// A racer ranking.
#[derive(Serialize)]
pub struct RacerRanking {
    pub rank: i32,
    pub id: String,
    pub name: String,
    /// Licence number
    pub ffv_id: String,
    /// Sailing club
    pub club_id: u32,
    /// Sailing club full name as string
    pub club_name: String,
    /// Sailing club abbrv name as string
    pub club_code: String,
    /// Net points
    pub points_net: f64,
    /// Total points
    pub points_total: f64,
    /// Races (rank, points, removed)
    pub races: Vec<(String, f64, bool)>,
}

impl RacerRanking {
    /// Initialize new Ranking
    pub fn new() -> RacerRanking {
        RacerRanking {
            rank: 0,
            id: String::new(),
            name: String::new(),
            ffv_id: String::new(),
            club_id: 0,
            club_name: String::new(),
            club_code: String::new(),
            points_net: 0.0,
            points_total: 0.0,
            races: Vec::new(),
        }
    }
}
