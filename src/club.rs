use serde::Serialize;

/// A sailing club
#[derive(Serialize)]
pub struct Club {
    pub id: u32,
    pub name: String,
    pub code: String,
    pub search_string: String,
}
