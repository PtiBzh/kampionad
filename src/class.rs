use serde::Serialize;

/// A ranking class
#[derive(Serialize, Debug)]
pub struct Class {
    pub id: u32,
    pub name: String,
    pub code: String,
    pub search_string: String,
}
