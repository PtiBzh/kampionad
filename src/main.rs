use std::fs::File;

use handlebars::{Helper, Context, RenderContext, Output, Handlebars, RenderError, to_json};
use scraper::{Html, Selector};
use serde_json::value::Map;

mod championship;
mod class;
mod club;
mod regatta;
mod regatta_class_result;
mod racer_ranking;
mod club_ranking;

use championship::Championship;
use racer_ranking::RacerRanking as Ranking;
use regatta::Regatta;
use regatta_class_result::RegattaClassResult;

/// Handlebars helper adding the correct number of races to group results header
fn for_races_helper(
    h: &Helper,
    _: &Handlebars,
    _: &Context,
    _: &mut RenderContext,
    out: &mut dyn Output,
) -> Result<(), RenderError> {
    let n = h
        .param(0)
        .and_then(|ref v| v.value().as_u64())
        .ok_or(RenderError::new(
            "Param 0 with u64 type is required for rank helper.",
        ))? as usize;
    for x in 0..n {
        out.write(&format!("& \\textbf{{{}}} ", x + 1)).unwrap();
    }
    Ok(())
}

/// Parse a racer ranking row element and return its values
fn parse_ranking(cs: &Championship, ranking_element: scraper::element_ref::ElementRef, races_total: u32) -> Ranking {
    // Rankings scraped from file
    let mut ranking = Ranking::new();

    // HTML selector
    let selector_cell = Selector::parse("td").unwrap();

    // Create iterator over each row's cells
    let mut cell_iter = ranking_element.select(&selector_cell);
    
    // Scrap rank
    ranking.rank = cell_iter
        .next().unwrap()
        .inner_html()
        .replace("<b> ", "")
        .replace("</b>", "")
        .trim()
        .parse().unwrap();

    // Scrap ID
    ranking.id = cell_iter
        .next().unwrap()
        .inner_html()
        .replace("<p>", "")
        .replace("</p>", "")
        .replace("&nbsp;", " ")
        .trim()
        .parse().unwrap();

    // Get name cell
    let name_cell: String;
    name_cell = cell_iter.next().unwrap().inner_html();

    // Slice licence number from name cell
    if name_cell.find("NoLicence=").is_some() {
        ranking.ffv_id = name_cell[name_cell.find("NoLicence=").unwrap()+10
                                   ..name_cell.find("&amp;").unwrap()].to_string();
    }

    // Slice name from name cell
    ranking.name = name_cell[name_cell.find("ze:9pt;\">").unwrap()+9
                             ..name_cell.find("</span").unwrap()].to_string();
    ranking.name = ranking.name.replace("&nbsp;", " ");

    // Scrap net points and convert it to hundrendth
    ranking.points_net = cell_iter
        .next().unwrap()
        .inner_html()
        .replace("<b>", "")
        .replace("</b>", "")
        .replace(".", "")
        .trim()
        .parse().unwrap();
    ranking.points_net = ranking.points_net / 100.0;

    // Scrap total points and convert it to hundrendth
    ranking.points_total = cell_iter
        .next().unwrap()
        .inner_html()
        .replace(".", "")
        .trim()
        .parse().unwrap();
    ranking.points_total = ranking.points_total / 100.0;

    // For each race, add rank and points to races vector
    for _ in 0..races_total {
        let mut race_cell: String;

        // Get race string
        race_cell = cell_iter.next().unwrap().inner_html();

        // Set race to removed if strike is present
        let removed = race_cell.find("strike").is_some();

        // Remove i tag
        race_cell = race_cell.replace("<i>", "");
        race_cell = race_cell.replace("</i>", "");

        // Remove striking
        race_cell = race_cell.replace("<strike>", "");
        race_cell = race_cell.replace("</strike>", "");

        // Remove medal color
        race_cell = race_cell.replace("<span style=\"background-color: #FFFFFF\">", "");
        race_cell = race_cell.replace("<span style=\"background-color: #FFFF00\">", "");
        race_cell = race_cell.replace("</span>", "");

        let rank = race_cell[..race_cell.find("<br>").unwrap()].to_string();
        let points = race_cell[race_cell.find("<br>").unwrap()+4..].trim().parse().unwrap();

        ranking.races.push((rank, points, removed));
    }
    
    // Scrap club name
    ranking.club_id = cs.get_club(cell_iter
        .next().unwrap()
        .inner_html());

    ranking.club_name = cs.clubs[ranking.club_id as usize].name.clone();
    ranking.club_code = cs.clubs[ranking.club_id as usize].code.clone();

    // Return scraped ranking
    ranking
}

/// Parse a group results element and return its values
fn parse_group(cs: &Championship, group: scraper::element_ref::ElementRef) -> RegattaClassResult {
    // Results scraped from element
    let mut group_result = RegattaClassResult::new();

    // html selectors
    let selector_infos = Selector::parse("b").unwrap();
    let selector_row = Selector::parse("tr").unwrap();

    // Get result infos
    let infos = group.select(&selector_infos).next().unwrap().inner_html();
    
    // Get group id and name
    group_result.class_id = cs.get_class(infos[27..32].to_string());
    group_result.class_name = cs.classes[group_result.class_id as usize].name.clone();

    // Scrap number of total races
    group_result.races_total = infos[infos.find("après").unwrap() + 6
                                     ..infos.find("courses").unwrap() - 1].trim().parse().unwrap();

    // Scrap number of races counted
    group_result.races_net = infos[infos.find("(").unwrap() + 1
                                   ..infos.find("retenu").unwrap() - 1].trim().parse().unwrap();

    // Ranking table row iterator
    let mut row_iter = group.select(&selector_row);

    // Pass header
    row_iter.next();

    // Parse each row after header
    for row in row_iter {
        group_result.add(parse_ranking(cs, row, group_result.races_total));
    }
    group_result
}

/// Parse a regatta results HTML file
fn parse_regatta(cs: &Championship, file: String) -> Regatta {
    // List of regatta's races results
    let mut regatta = Regatta::new(1, "today".to_string());

    let fragment = Html::parse_fragment(&file);

    // HTML selector
    let selector_group = Selector::parse("div center").unwrap();

    // Tables iterator
    let group_iter = fragment.select(&selector_group);

    // For each center markup
    for group in group_iter {
        // Parse only center containing results
        if group.inner_html().find("size=\"2\"").is_none() {
            regatta.add_results(parse_group(&cs, group));
        }
    }

    // Calculate rankings
    regatta.calculate_clubs_rankings(cs);

    // Return regatta
    regatta
}

// Program main function
pub fn main() {
    // Impl write trait to vector of u8
    use std::io::prelude::*;

    // Calculated championship
    let mut cs = championship::Championship::new();

    // Add VBMx sailing clubs
    cs.add_club("S. R. Térénez".to_string(), "SRTZ".to_string(), "TERENEZ".to_string());
    cs.add_club("É. V. Locquirec".to_string(), "EVL".to_string(), "LOCQUIREC".to_string());
    cs.add_club("A. S. V. Plouescat".to_string(), "ASVP".to_string(), "PLOUESCAT".to_string());
    cs.add_club("C. N. Saint-Pol".to_string(), "CNSP".to_string(), "POL".to_string());
    cs.add_club("C. N. Roscoff".to_string(), "CNR".to_string(), "ROSCOFF".to_string());
    cs.add_club("C. N. Carantec".to_string(), "CNCa".to_string(), "CARANTEC".to_string());
    cs.add_club("C. N. Clederois".to_string(), "CNCl".to_string(), "CLEDER".to_string());

    // Add VBMx sailing classes
    cs.add_class("Catamaran".to_string(), "CAT".to_string(), "CATAM".to_string());
    cs.add_class("Dériveur".to_string(), "DER".to_string(), "DERIV".to_string());
    cs.add_class("Optimist 1ère année".to_string(), "OP1".to_string(), "OPTI".to_string());
    cs.add_class("Optimist".to_string(), "OPE".to_string(), "OPTIM".to_string());
    cs.add_class("Planche 1ère année".to_string(), "PA1".to_string(), "PAV 1".to_string());
    cs.add_class("Planche".to_string(), "PAV".to_string(), "PLANC".to_string());

    // Group classes
    cs.add_club_ranking_classes(vec!((cs.get_class_from_code("OPE".to_string()), 0),
                          (cs.get_class_from_code("OP1".to_string()), 1)));

    cs.add_club_ranking_classes(vec!((cs.get_class_from_code("PAV".to_string()), 0),
                          (cs.get_class_from_code("PA1".to_string()), 1)));

    // Read from HTML file, convert to utf-8
    let doc: String = std::fs::read("iso8859-1.html").unwrap().iter().map(|&c| c as char).collect();

    cs.add_regatta(parse_regatta(&cs, doc));

    // Handlebars instance
    let mut handlebars = Handlebars::new();
    // Data for handlebars template
    let mut data = Map::new();

    // Handlebars helper adding correct number of races to group header
    handlebars.register_helper("for_races", Box::new(for_races_helper));

    // Insert all group results to handlebars instance
    data.insert("championship".to_string(), to_json(&cs));

    // Template file
    let source_template = include_str!("kampionad.tex");
    // Buffer for rendered file
    let mut rendered_buffer: Vec<u8> = Vec::new();
    // Rendered file destination
    let mut output_file = File::create("kampionad.pdf").unwrap();

    // Use rankings datas to render latex/handlebars template
    handlebars.render_template_source_to_write(&mut source_template.as_bytes(), &data, &mut rendered_buffer).unwrap();

    // Render to pdf
    output_file.write(&tectonic::latex_to_pdf(std::str::from_utf8(&rendered_buffer).unwrap()).unwrap()).unwrap();
}


