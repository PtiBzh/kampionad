use serde::Serialize;

use super::championship::Championship;
use super::club_ranking::ClubRanking;
use super::regatta_class_result::RegattaClassResult;

/// A regatta
#[derive(Serialize)]
pub struct Regatta {
    id: u32,
    date: String,
    class_results: Vec<RegattaClassResult>,
    clubs_rankings: Vec<ClubRanking>,
}

impl Regatta {
    /// Initialize a new regatta
    pub fn new(id: u32, date: String) -> Regatta {
        Regatta {
            id: id,
            date: date,
            class_results: Vec::new(),
            clubs_rankings: Vec::new(),
        }
    }

    /// Add a new class result to the regatta
    pub fn add_results(&mut self, result: RegattaClassResult) {
        self.class_results.push(result);
    }

    /// Calculate clubs rankings
    pub fn calculate_clubs_rankings(&mut self, cs: &Championship) {
        // For each real clubs
        for club in &cs.clubs {
            if club.id != 0 {
                // New calculated ranking
                let new_ranking = ClubRanking::new();

                //
            }
        }
    }
}
