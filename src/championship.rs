use serde::Serialize;

use super::club::Club;
use super::club_ranking::ClubRanking;
use super::class::Class;
use super::regatta::Regatta;

/// A championship
#[derive(Serialize)]
pub struct Championship {
    name: String,
    edition: String,
    pub clubs: Vec<Club>,
    pub classes: Vec<Class>,
    // list of classes counted in clubs ranking (class id, class weight)
    club_ranking_classes: Vec<Vec<(u32, u32)>>,
    regattas: Vec<Regatta>,
}

impl Championship {
    /// Initialize a new championship
    pub fn new() -> Championship {
        let mut new_cs = Championship {
            name: String::new(),
            edition: String::new(),
            clubs: Vec::new(),
            classes: Vec::new(),
            club_ranking_classes: Vec::new(),
            regattas: Vec::new(),
        };
        new_cs.add_club("".to_string(), "".to_string(), "".to_string());
        new_cs
    }

    /// Add new club
    pub fn add_club(&mut self, name: String, code: String, search_string: String) {
        self.clubs.push(
            Club {
                id: self.clubs.len() as u32,
                name: name,
                code: code,
                search_string: search_string,
            }
        );
    }

    /// Get club id from string
    pub fn get_club(&self, string: String) -> u32 {
        let mut id: u32 = 0;
        for club in &self.clubs {
            if string.find(&club.search_string).is_some() {
                id = club.id;
            }
        }
        id
    }

    /// Add new ranking class
    pub fn add_class(&mut self, name: String, code: String, search_string: String) {
        self.classes.push(
            Class {
                id: self.classes.len() as u32,
                name: name,
                code: code,
                search_string: search_string,
            }
        );

        // Add this class to list of results classes
        self.add_club_ranking_classes(vec!((self.classes.len()  as u32 - 1, 0)));
    }

    /// Get ranking class id from string
    pub fn get_class(&self, string: String) -> u32 {
        let mut id: u32 = 0;
        for class in &self.classes {
            if string.find(&class.search_string).is_some() {
                id = class.id;
            }
        }
        id
    }

    /// Get ranking class id from class code
    pub fn get_class_from_code(&self, code: String) -> u32 {
        let mut id: u32 = 0;
        for class in &self.classes {
            if class.code == code {
                id = class.id;
            }
        }
        id
    }

    /// Add this class or classes to list of classes counted in club ranking
    pub fn add_club_ranking_classes(&mut self, classes: Vec<(u32, u32)>) {
        // Get id of class to remove
        for class in &classes {
            for (i, cla) in self.club_ranking_classes.clone().iter().enumerate() {
                if cla[0].0 == class.0 {
                    self.club_ranking_classes.remove(i);
                }
            }
        }

        self.club_ranking_classes.push(classes);
  }

    /// Add a regatta to this championship
    pub fn add_regatta(&mut self, regatta: Regatta) {
        self.regattas.push(regatta);
    }
}
