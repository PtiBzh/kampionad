use serde::Serialize;

/// A club ranking.
#[derive(Serialize)]
pub struct ClubRanking {
    pub rank: i32,
    pub club_id: String,
    /// Sailing club full name as string
    pub club_name: String,
    /// Net points
    pub points_net: f64,
    /// Total points
    pub points_total: f64,
    /// Points per classes (places, points, removed)
    pub points_per_classes: Vec<(u32, f64, bool)>,
}

impl ClubRanking {
    /// Initialize new Ranking
    pub fn new() -> ClubRanking {
        ClubRanking {
            rank: 0,
            club_id: String::new(),
            club_name: String::new(),
            points_net: 0.0,
            points_total: 0.0,
            points_per_classes: Vec::new(),
        }
    }
}
