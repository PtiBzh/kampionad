use serde::Serialize;

use super::racer_ranking::RacerRanking;
use super::club_ranking::ClubRanking;

/// Regatta results for a class.
#[derive(Serialize)]
pub struct RegattaClassResult {
    /// Class name
    pub class_name: String,
    /// Class id
    pub class_id: u32,
    /// Number of racers
    registrants: u32,
    /// Total number of races
    pub races_total: u32,
    /// Number of races keeped for ranking
    pub races_net: u32,
    /// Racers rankings
    racers_rankings: Vec<RacerRanking>,
    /// Clubs rankings
    clubs_rankings: Vec<ClubRanking>,
}

impl RegattaClassResult {
    /// Initialize an new class result
    pub fn new() -> RegattaClassResult {
        RegattaClassResult{
            class_name: String::new(),
            class_id: 0,
            registrants: 0,
            races_total: 0,
            races_net: 0,
            racers_rankings: Vec::new(),
            clubs_rankings: Vec::new(),
        }
    }

    /// Add a new racer ranking
    pub fn add(&mut self, ranking: RacerRanking) {
        self.racers_rankings.push(ranking);
        self.registrants = self.registrants + 1;
    }
}
